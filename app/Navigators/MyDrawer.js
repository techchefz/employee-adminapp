import React from 'react';
import { StyleSheet, Text, View, Button, Headers, AsyncStorage} from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import AddProject from '../screens/AddProject';
import LogOut from '../screens/LogOut';

export let MyDrawer = createDrawerNavigator({
  Home: {
    screen: HomeScreen,
  },
  Add: {
    screen: AddProject,
  },
  LogOut : {
    screen: LogOut
  }
});
