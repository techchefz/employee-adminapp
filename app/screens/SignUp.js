import React from 'react';
import { View, Text ,KeyboardAvoidingView, StyleSheet,TextInput, Label, Button,  ActivityIndicator } from 'react-native';
import IP from '../ip';

export default class SignUp extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            name:'',
            email:'',
            password:'',
        }
     }

    signUp(){
        fetch( 'http://'+IP+'/signup', {
        method: 'POST',
        headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            name: this.state.name.toLowerCase(),
            email: this.state.email.toLowerCase(),
            password:this.state.password,
            userType: 'admin'
        }),
    })
    .then((res)=>{
        if(res.status == 200){
            alert('Successfully Signed UP');
            this.props.navigation.navigate('Login');
        }else{
            alert('Something went wrong \n Try Again!')
        }
    });
    }

     render() {
        return (
          <View style={styles.container}>
          <View style={{ marginTop: '20%'  }}>
            <Text> Name</Text>
              <TextInput
              placeholder='Name'
              onChangeText={ (uname) => this.setState({name: uname}) }
               style={styles.input}
               />
            <Text> Email</Text>
               <TextInput
              placeholder='Email'
              onChangeText={ (email ) => { this.setState({email : email})}}
              style={styles.input}
               />
            <Text> Password</Text>
              <TextInput
              placeholder='Password'
              onChangeText={ (password ) => { this.setState({password : password})}}
              secureTextEntry
               style={styles.input}
               />
              <Button title='SignUp' onPress={()=>this.signUp()}/>
              </View>
          </View>
        );
      }
}

const styles =StyleSheet.create({
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 20,
        color: 'black',
        paddingHorizontal: 10
    }
});
