import React, { Component } from 'react';
import { StyleSheet ,View ,TextInput, Text, Button, AsyncStorage } from 'react-native'; 
import IP from '../ip';

export default class LoginForm extends Component {
    constructor(props){
      super(props);
        this.state={
            uname : '',
            password :'',
        }
  }

  
   async checkCredentials(){
    fetch( 'http://'+IP+'/login', {
        method: 'POST',
        headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            email: this.state.uname.toLowerCase(),
            password:this.state.password,
            userType: 'admin'
        }),
    })
    .then((res)=>{
        if(res.status == 200){
            AsyncStorage.setItem('uname',JSON.stringify(this.state.uname))
            .then( ()=>{ console.log('Uname Saved') })
            .catch(()=>{ console.log('Error occured while saving Uname') });
            
        this.props.navigation.navigate('Dashboard');
        }else if( res.status == 400 ){
            alert("User Not Registered");
        }else if( res.status == 401){
            alert("Incorrect Password")
        }
    });
}
    
    render() {
    return (
      <View style={styles.container}>
      <View>
          <Text>
              Admin LOGS APP
          </Text>
      </View>
          <TextInput
          placeholder = 'Admin Email'
          onChangeText={ (uname) => this.setState({uname: uname}) }
           style={styles.input}
           />
          <TextInput
          placeholder ='Password'
          onChangeText={ (password ) => { this.setState({password : password})}}
          secureTextEntry
          style={styles.input}
           />
          <Button title='Login' onPress={()=>this.checkCredentials()}/>
      </View>
    );
  }
}

const styles =StyleSheet.create({
    container: {
        padding: 20,
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginBottom: 20,
        color: 'black',
        paddingHorizontal: 10
    }
});
