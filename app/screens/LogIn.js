import React , { Component } from 'react';
import { StyleSheet, View , Image, Text, KeyboardAvoidingView } from 'react-native';
import  LoginForm  from '../screens/LoginForm';

export default class LogIn extends Component {
    render(){
        return(
            <KeyboardAvoidingView 
             behavior="padding" 
             enabled 
             style={ styles.container } >
                <View style={styles.logoContainer}>
                    <Image 
                    style={styles.logo}
                    source={require('../images/logo.png')}
                    />
                </View>
                <View >
                    <LoginForm navigation={this.props.navigation}/>
                </View>
                <View  style={{ justifyContent: 'center', alignItems: 'center', flexDirection:'row' }}>
                    <Text  >
                        Not Registered ? 
                    </Text>
                    <Text  style={{ color: 'blue'}}
                    onPress={() =>{ this.props.navigation.navigate('SignUP')}} >
                    SignUp
                    </Text>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    logoContainer :{
        height:'50%',
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'transparent',
    },
    logo : {
        width : '50%',
        height :'50%',
        resizeMode:'contain',
    },
    
});