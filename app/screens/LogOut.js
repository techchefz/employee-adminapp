import React from 'react';
import { View, AsyncStorage } from 'react-native';

export default class LogOut extends React.Component{
    constructor(props) {
        super(props);
     }

    componentWillMount() {
        AsyncStorage.clear()
        .then(()=>{
            alert('You are Logged Out ');
            this.props.navigation.navigate('Login');
        })
        .catch(()=>{ 
            alert('Error Logging Out \n Try Again');
            this.props.navigation.navigate('Home');
        });
    }

    render(){
        return(
            <View>
            </View>
        );
    }
}