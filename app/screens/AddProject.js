import React from 'react';
import {View, ScrollView, TextInput, StyleSheet,Text,Button, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import { Header, Card,  } from 'react-native-elements';
import IP from '../ip';

export default class AddProject extends React.Component{
  
  constructor(props){
    super(props);
    this.state={
      newProject: '',
      projects:[],
    };
  }

  async save(){
   fetch( 'http://'+IP+'/add-project', {
        method: 'POST',
        headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
         },
         body: JSON.stringify({
          project : this.state.newProject,
        }),
    })
    .then((res)=>{
       if(res.status ==  200 ){
        alert('Project Added');
        this.props.navigation.navigate('Home');
        }else{
          alert('Error Adding');
        }
    })
    .catch(()=>{
      console.log('Try Again');
    });
    this.props.navigation.navigate('Home');
  }

  render(){
        return(
        <View>
          <Header
          placement="left"
          leftComponent={{ icon: 'menu', color: '#fff' , onPress: ()=>this.props.navigation.toggleDrawer() }}
          centerComponent={{ text: 'Update Work', style: { color: '#fff' } }}
          />
        <ScrollView>
          <Card title = 'Add Project' >
            <Text>Add Project</Text>
            <TextInput
              placeholder='Project Name'
              onChangeText={ (name) => this.setState({newProject: name}) }
              style={styles.input}
            />
            <Button title='Save' onPress={()=>{this.save()}} /> 
          </Card>
        </ScrollView>
      </View>
      );
    }
}

const styles =StyleSheet.create({
  input: {
      height: 40,
      backgroundColor: 'rgba(255,255,255,0.2)',
      marginBottom: 20,
      color: 'black',
      paddingHorizontal: 10
  }
});
