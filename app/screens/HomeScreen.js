import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Header, Card, Button } from 'react-native-elements';

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: [],
      userLogs: [],
      userWorkDetails: [],
      showLogs: false,
      showProjectWiseTime: false,
    }
  }

  componentWillMount() {
    this.loadUserDetails();
  }


  loadUserDetails() {
    fetch('http://' + IP + '/get-user-details')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ userDetails: responseJson });
      })
      .catch((error) => {
        console.log('Error fetching user details');
      });
  }

 async requestUserLogs(employee) {
   await this.setState({showLogs: true});
    fetch('http://' + IP + '/get-user-log', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user: employee
      }),
    }).then((res) => res.json()).then((result) => {
      if (result.success == true) {
        this.setState({ userLogs: result.data });
      } else {
        alert(result.data.message);
      }
    }).catch((e) => {
      alert('error fetching user logs');
    });
    return;
  }

 async countHoursOnEachProject(emp) {
   await this.setState({ showProjectWiseTime : true });
   let data = this.state.userLogs;
    let work = [];

    for (let i = 0; i < data.length; i++) {
      let obj = data[i];
      if (work.length == 0) {
        work.push({ 'name': obj.project, 'hrs': obj.hours });
      }
      else {
        for (let j = 0; j < work.length; i++) {
          if (work[j].name == obj.project) {
            work[j].hrs = work[j].hrs + obj.hours;
            break;
          } else {
            work.push({ 'name': obj.project, 'hrs': obj.hours });
            break;
          }
        }
      }
    }
    this.setState({ userWorkDetails: work });
    
  }

  render() {
    return (
      <View>
        <Header
          placement="left"
          leftComponent={{ icon: 'menu', color: '#fff', onPress: () => this.props.navigation.toggleDrawer() }}
          centerComponent={{ text: 'Home', style: { color: '#fff' } }}
        />
        <ScrollView>
          {
            this.state.userDetails.map((user, i) => {
              return (
                <ScrollView key={'Card'+i }>
                <Card title={user.name} key={i}>
                  <View key={i + i * 19} >
                    <Text>Email ID : {user.email}</Text>
                    <Text>ID : {user.empID}</Text>
                    {
                      this.state.showLogs ? 
                      <Button title='Hide' onPress={() => { this.setState({showLogs : false }) }} />
                      :
                      <Button title='View' onPress={() => { this.requestUserLogs(user.email) }} />
                    }
                    {
                      this.state.showProjectWiseTime ?
                      <Button title='Hide' onPress={() => { this.setState({showProjectWiseTime : false}) }} />
                      :
                      <Button title='ProjectWise Time' onPress={() => { this.countHoursOnEachProject(user.email) }} />
                    }
                    
                  </View>
                  <ScrollView>
                  {
                    this.state.showLogs ?  this.state.userLogs.map((detail, i) => {
                      return (
                        <Card title={detail.date} key={i}>
                          <View key={i + i * 17} >
                            <Text>Project : {detail.project}</Text>
                            <Text>Hours : {detail.hours}</Text>
                            <Text>Activities : {detail.activities} </Text>
                          </View>
                        </Card>
                      );
                    }) : null
                  }
                  {
                    this.state.showProjectWiseTime ? 

                      this.state.userWorkDetails.map((proWork, i) => {
                        return (
                          <Card title={'Project '+(parseInt(i)+1)} key={'project'+i} >
                          <Text key={i + i * 7}>{"Name : "+proWork.name}</Text>
                          <Text key={i + i * 37}>{"Total Time : " + proWork.hrs + " hrs"}</Text>
                          </Card>
                        );
                      }) : null
                  }
                  </ScrollView>
                </Card>
              </ScrollView>  
              );
            })
          }
        </ScrollView>
      </View>
    );
  }
}

