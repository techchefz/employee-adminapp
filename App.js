import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {StackNavigator} from "./app/Navigators/stackNavigator";

export default class App extends React.Component {
  render() {
    console.disableYellowBox = true;
    return (
     <StackNavigator />
    );
  }
}
